Defaults::Application.routes.draw do
  devise_for :users

  resources :news do
    get "default", on: :collection
    put "set_default", on: :member
  end

  root to: "news#default"
end
