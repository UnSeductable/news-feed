require "spec_helper"

describe News do
  let!(:n) { FactoryGirl.create(:news) }

  it "becomes default after method call" do
    n.default!
    n.should be_default
  end

  it "has a correct default record" do
    n.default!
    News.default.should == n
  end

  it "is default if it is the only record" do
    n.should be_default
  end

  context "with empty table" do
    before { n.destroy }

    it "has no defaultings in empty table" do
      ActsAsDefaultable::Defaulting.where(defaulting_type: News.to_s).should be_empty
    end

    it "has no default record in empty table" do
      News.default.should be_nil
    end
  end

  context "with multiple records" do
    let(:other_n) { FactoryGirl.create(:news) }
    before { other_n.default! }

    it "makes new record as default" do
      other_n.should be_default
    end

    it "unsets old default record" do
      n.should_not be_default
    end

    it "has a correct default record" do
      News.default.should == other_n
    end

    it "sets old record as default after deleting the new one" do
      other_n.destroy
      n.should be_default
    end
  end
end
