FactoryGirl.define do
  factory :news do |f|
    f.title "Title"
    f.body "Sample text"
  end
end
