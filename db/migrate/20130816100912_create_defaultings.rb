class CreateDefaultings < ActiveRecord::Migration
  def change
    create_table :defaultings do |t|
      t.integer :defaulting_id
      t.string :defaulting_type

      t.timestamps
    end
    add_index :defaultings, :defaulting_type, unique: true
  end
end
