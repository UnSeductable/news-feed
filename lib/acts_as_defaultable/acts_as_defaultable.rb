module ActsAsDefaultable
  module Methods
    def self.included(base)
      base.extend(ClassMethods)
      base.after_create :set_default_record
      base.after_destroy :set_default_record
    end

    # Class methods for defaultable models
    module ClassMethods
      # Get a default object of current model
      def default
        defaulting = Defaulting.where(defaulting_type: self.to_s).first
        self.where(id: defaulting.defaulting_id).first if defaulting
      end
    end

    # Check if it is default
    def default?
      Defaulting.where(defaulting_id: self.id, defaulting_type: self.class.to_s).present?
    end

    # Set current object as default
    def default!
      defaulting_record = Defaulting.where(defaulting_type: self.class.to_s).first
      defaulting_record.destroy if defaulting_record

      Defaulting.create(defaulting_id: self.id, defaulting_type: self.class.to_s)
    end

    # Set record as default if it is created in empty table
    # or delete defaultings if table becomes empty
    def set_default_record
      if self.class.default.nil?
        # If no default and table isn't empty
        # set the first record as default
        if self.class.count > 0
          self.class.last.default!
        # Otherwise delete defaulting
        else
          Defaulting.where(defaulting_type: self.class.to_s).first.destroy
        end
      end
    end

  end

  # Defaulting presents a model which saves
  # @defaulting_type - name of defaultable model (e.g. News.to_s)
  # @defaulting_id - id of the model's default record
  class Defaulting < ActiveRecord::Base
    attr_accessible :defaulting_id, :defaulting_type
  end
end