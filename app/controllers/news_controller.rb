class NewsController < ApplicationController
  before_filter :authenticate_user!, except: [:index, :show, :default]

  def index
    @news = News.order("created_at DESC").page(params[:page]).per(5)
  end

  def show
    @news = News.find params[:id]
  end

  def new
    @news = News.new
  end

  def create
    @news = News.new params[:news]
    if @news.save
      redirect_to news_path(@news)
    else
      render "new"
    end
  end

  def edit
    @news = News.find params[:id]
  end

  def update
    @news = News.find params[:id]
    if @news.update_attributes params[:news]
      redirect_to news_path(@news)
    else
      render "edit"
    end
  end

  def destroy
    News.find(params[:id]).destroy
    redirect_to news_index_path
  end

  def default
    @news = News.default
    render "show"
  end

  def set_default
    @news = News.find params[:id]
    @news.default!
    redirect_to news_index_path
  end
end
