class News < ActiveRecord::Base
  attr_accessible :body, :title

  # See lib/acts_as_defaultable module
  include ActsAsDefaultable::Methods
end
