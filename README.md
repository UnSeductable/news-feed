# News feed
## Installation guide
Follow the process below

    git clone git@bitbucket.org:UnSeductable/news-feed.git
    cd news-feed
    rake db:migrate
    rake db:seed

## Accessing admin panel
Click on the login button and enter these credentials:

    email: sample@example.com
    password: weakpassword

You can also visit the application at [http://limitless-bastion-4706.herokuapp.com](http://limitless-bastion-4706.herokuapp.com)

## Enjoy :)
